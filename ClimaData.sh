#!/bin/bash

# Define as cidades
cidades=(João Pessoa Recife)

# Obtém o clima de cada cidade
clima_joao_pessoa=$(curl -s "wttr.in/João+Pessoa?format=%C+%t+%w")

#definir data e hora
data_hora=$(date +'%d/%m/%Y %H:%M')

#definir clima recife
clima_recife=$(curl -s "wttr.in/Recife?format=%C+%t+%w")

# Exibe o clima
echo "Cidade: João Pessoa | Clima: ${clima_joao_pessoa} | Data e hora: ${data_hora}"
echo "Cidade: Recife | Clima: ${clima_recife} | Data e hora: ${data_hora}"
